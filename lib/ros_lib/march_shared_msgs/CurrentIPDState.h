#ifndef _ROS_march_shared_msgs_CurrentIPDState_h
#define _ROS_march_shared_msgs_CurrentIPDState_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "std_msgs/Header.h"

namespace march_shared_msgs
{

  class CurrentIPDState : public ros::Msg
  {
    public:
      typedef std_msgs::Header _header_type;
      _header_type header;
      typedef const char* _menu_name_type;
      _menu_name_type menu_name;

    CurrentIPDState():
      header(),
      menu_name("")
    {
    }

    virtual int serialize(unsigned char *outbuffer) const override
    {
      int offset = 0;
      offset += this->header.serialize(outbuffer + offset);
      uint32_t length_menu_name = strlen(this->menu_name);
      varToArr(outbuffer + offset, length_menu_name);
      offset += 4;
      memcpy(outbuffer + offset, this->menu_name, length_menu_name);
      offset += length_menu_name;
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer) override
    {
      int offset = 0;
      offset += this->header.deserialize(inbuffer + offset);
      uint32_t length_menu_name;
      arrToVar(length_menu_name, (inbuffer + offset));
      offset += 4;
      for(unsigned int k= offset; k< offset+length_menu_name; ++k){
          inbuffer[k-1]=inbuffer[k];
      }
      inbuffer[offset+length_menu_name-1]=0;
      this->menu_name = (char *)(inbuffer + offset-1);
      offset += length_menu_name;
     return offset;
    }

    virtual const char * getType() override { return "march_shared_msgs/CurrentIPDState"; };
    virtual const char * getMD5() override { return "5c8f672f3bd0ce55b55ef09e312dd58c"; };

  };

}
#endif
