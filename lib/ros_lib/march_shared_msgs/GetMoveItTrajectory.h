#ifndef _ROS_SERVICE_GetMoveItTrajectory_h
#define _ROS_SERVICE_GetMoveItTrajectory_h
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "trajectory_msgs/JointTrajectory.h"
#include "sensor_msgs/JointState.h"
#include "geometry_msgs/Pose.h"

namespace march_shared_msgs
{

static const char GETMOVEITTRAJECTORY[] = "march_shared_msgs/GetMoveItTrajectory";

  class GetMoveItTrajectoryRequest : public ros::Msg
  {
    public:
      typedef const char* _swing_leg_type;
      _swing_leg_type swing_leg;
      typedef geometry_msgs::Pose _swing_leg_target_pose_type;
      _swing_leg_target_pose_type swing_leg_target_pose;
      typedef sensor_msgs::JointState _stance_leg_target_type;
      _stance_leg_target_type stance_leg_target;

    GetMoveItTrajectoryRequest():
      swing_leg(""),
      swing_leg_target_pose(),
      stance_leg_target()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const override
    {
      int offset = 0;
      uint32_t length_swing_leg = strlen(this->swing_leg);
      varToArr(outbuffer + offset, length_swing_leg);
      offset += 4;
      memcpy(outbuffer + offset, this->swing_leg, length_swing_leg);
      offset += length_swing_leg;
      offset += this->swing_leg_target_pose.serialize(outbuffer + offset);
      offset += this->stance_leg_target.serialize(outbuffer + offset);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer) override
    {
      int offset = 0;
      uint32_t length_swing_leg;
      arrToVar(length_swing_leg, (inbuffer + offset));
      offset += 4;
      for(unsigned int k= offset; k< offset+length_swing_leg; ++k){
          inbuffer[k-1]=inbuffer[k];
      }
      inbuffer[offset+length_swing_leg-1]=0;
      this->swing_leg = (char *)(inbuffer + offset-1);
      offset += length_swing_leg;
      offset += this->swing_leg_target_pose.deserialize(inbuffer + offset);
      offset += this->stance_leg_target.deserialize(inbuffer + offset);
     return offset;
    }

    virtual const char * getType() override { return GETMOVEITTRAJECTORY; };
    virtual const char * getMD5() override { return "90f3814490890f393729ecff105a02dc"; };

  };

  class GetMoveItTrajectoryResponse : public ros::Msg
  {
    public:
      typedef bool _success_type;
      _success_type success;
      typedef trajectory_msgs::JointTrajectory _trajectory_type;
      _trajectory_type trajectory;

    GetMoveItTrajectoryResponse():
      success(0),
      trajectory()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const override
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_success;
      u_success.real = this->success;
      *(outbuffer + offset + 0) = (u_success.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->success);
      offset += this->trajectory.serialize(outbuffer + offset);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer) override
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_success;
      u_success.base = 0;
      u_success.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->success = u_success.real;
      offset += sizeof(this->success);
      offset += this->trajectory.deserialize(inbuffer + offset);
     return offset;
    }

    virtual const char * getType() override { return GETMOVEITTRAJECTORY; };
    virtual const char * getMD5() override { return "9dac17c68dbaa65fdf53974580600f37"; };

  };

  class GetMoveItTrajectory {
    public:
    typedef GetMoveItTrajectoryRequest Request;
    typedef GetMoveItTrajectoryResponse Response;
  };

}
#endif
