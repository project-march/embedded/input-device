#ifndef _ROS_march_shared_msgs_JointValues_h
#define _ROS_march_shared_msgs_JointValues_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "control_msgs/FollowJointTrajectoryFeedback.h"

namespace march_shared_msgs
{

  class JointValues : public ros::Msg
  {
    public:
      typedef control_msgs::FollowJointTrajectoryFeedback _controller_output_type;
      _controller_output_type controller_output;
      uint32_t velocities_length;
      typedef float _velocities_type;
      _velocities_type st_velocities;
      _velocities_type * velocities;
      uint32_t accelerations_length;
      typedef float _accelerations_type;
      _accelerations_type st_accelerations;
      _accelerations_type * accelerations;
      uint32_t jerks_length;
      typedef float _jerks_type;
      _jerks_type st_jerks;
      _jerks_type * jerks;

    JointValues():
      controller_output(),
      velocities_length(0), st_velocities(), velocities(nullptr),
      accelerations_length(0), st_accelerations(), accelerations(nullptr),
      jerks_length(0), st_jerks(), jerks(nullptr)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const override
    {
      int offset = 0;
      offset += this->controller_output.serialize(outbuffer + offset);
      *(outbuffer + offset + 0) = (this->velocities_length >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->velocities_length >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->velocities_length >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->velocities_length >> (8 * 3)) & 0xFF;
      offset += sizeof(this->velocities_length);
      for( uint32_t i = 0; i < velocities_length; i++){
      offset += serializeAvrFloat64(outbuffer + offset, this->velocities[i]);
      }
      *(outbuffer + offset + 0) = (this->accelerations_length >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->accelerations_length >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->accelerations_length >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->accelerations_length >> (8 * 3)) & 0xFF;
      offset += sizeof(this->accelerations_length);
      for( uint32_t i = 0; i < accelerations_length; i++){
      offset += serializeAvrFloat64(outbuffer + offset, this->accelerations[i]);
      }
      *(outbuffer + offset + 0) = (this->jerks_length >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->jerks_length >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->jerks_length >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->jerks_length >> (8 * 3)) & 0xFF;
      offset += sizeof(this->jerks_length);
      for( uint32_t i = 0; i < jerks_length; i++){
      offset += serializeAvrFloat64(outbuffer + offset, this->jerks[i]);
      }
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer) override
    {
      int offset = 0;
      offset += this->controller_output.deserialize(inbuffer + offset);
      uint32_t velocities_lengthT = ((uint32_t) (*(inbuffer + offset))); 
      velocities_lengthT |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1); 
      velocities_lengthT |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2); 
      velocities_lengthT |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3); 
      offset += sizeof(this->velocities_length);
      if(velocities_lengthT > velocities_length)
        this->velocities = (float*)realloc(this->velocities, velocities_lengthT * sizeof(float));
      velocities_length = velocities_lengthT;
      for( uint32_t i = 0; i < velocities_length; i++){
      offset += deserializeAvrFloat64(inbuffer + offset, &(this->st_velocities));
        memcpy( &(this->velocities[i]), &(this->st_velocities), sizeof(float));
      }
      uint32_t accelerations_lengthT = ((uint32_t) (*(inbuffer + offset))); 
      accelerations_lengthT |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1); 
      accelerations_lengthT |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2); 
      accelerations_lengthT |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3); 
      offset += sizeof(this->accelerations_length);
      if(accelerations_lengthT > accelerations_length)
        this->accelerations = (float*)realloc(this->accelerations, accelerations_lengthT * sizeof(float));
      accelerations_length = accelerations_lengthT;
      for( uint32_t i = 0; i < accelerations_length; i++){
      offset += deserializeAvrFloat64(inbuffer + offset, &(this->st_accelerations));
        memcpy( &(this->accelerations[i]), &(this->st_accelerations), sizeof(float));
      }
      uint32_t jerks_lengthT = ((uint32_t) (*(inbuffer + offset))); 
      jerks_lengthT |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1); 
      jerks_lengthT |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2); 
      jerks_lengthT |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3); 
      offset += sizeof(this->jerks_length);
      if(jerks_lengthT > jerks_length)
        this->jerks = (float*)realloc(this->jerks, jerks_lengthT * sizeof(float));
      jerks_length = jerks_lengthT;
      for( uint32_t i = 0; i < jerks_length; i++){
      offset += deserializeAvrFloat64(inbuffer + offset, &(this->st_jerks));
        memcpy( &(this->jerks[i]), &(this->st_jerks), sizeof(float));
      }
     return offset;
    }

    virtual const char * getType() override { return "march_shared_msgs/JointValues"; };
    virtual const char * getMD5() override { return "8c2d87ce75da8d6f89c63a524cb72481"; };

  };

}
#endif
