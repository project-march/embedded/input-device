#ifndef _ROS_march_shared_msgs_MotorControllerState_h
#define _ROS_march_shared_msgs_MotorControllerState_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "std_msgs/Header.h"

namespace march_shared_msgs
{

  class MotorControllerState : public ros::Msg
  {
    public:
      typedef std_msgs::Header _header_type;
      _header_type header;
      uint32_t joint_names_length;
      typedef char* _joint_names_type;
      _joint_names_type st_joint_names;
      _joint_names_type * joint_names;
      uint32_t error_status_length;
      typedef char* _error_status_type;
      _error_status_type st_error_status;
      _error_status_type * error_status;
      uint32_t operational_state_length;
      typedef char* _operational_state_type;
      _operational_state_type st_operational_state;
      _operational_state_type * operational_state;
      uint32_t motor_current_length;
      typedef float _motor_current_type;
      _motor_current_type st_motor_current;
      _motor_current_type * motor_current;
      uint32_t motor_voltage_length;
      typedef float _motor_voltage_type;
      _motor_voltage_type st_motor_voltage;
      _motor_voltage_type * motor_voltage;
      uint32_t absolute_position_iu_length;
      typedef float _absolute_position_iu_type;
      _absolute_position_iu_type st_absolute_position_iu;
      _absolute_position_iu_type * absolute_position_iu;
      uint32_t incremental_position_iu_length;
      typedef float _incremental_position_iu_type;
      _incremental_position_iu_type st_incremental_position_iu;
      _incremental_position_iu_type * incremental_position_iu;
      uint32_t absolute_velocity_iu_length;
      typedef float _absolute_velocity_iu_type;
      _absolute_velocity_iu_type st_absolute_velocity_iu;
      _absolute_velocity_iu_type * absolute_velocity_iu;
      uint32_t incremental_velocity_iu_length;
      typedef float _incremental_velocity_iu_type;
      _incremental_velocity_iu_type st_incremental_velocity_iu;
      _incremental_velocity_iu_type * incremental_velocity_iu;
      uint32_t absolute_position_length;
      typedef float _absolute_position_type;
      _absolute_position_type st_absolute_position;
      _absolute_position_type * absolute_position;
      uint32_t incremental_position_length;
      typedef float _incremental_position_type;
      _incremental_position_type st_incremental_position;
      _incremental_position_type * incremental_position;
      uint32_t absolute_velocity_length;
      typedef float _absolute_velocity_type;
      _absolute_velocity_type st_absolute_velocity;
      _absolute_velocity_type * absolute_velocity;
      uint32_t incremental_velocity_length;
      typedef float _incremental_velocity_type;
      _incremental_velocity_type st_incremental_velocity;
      _incremental_velocity_type * incremental_velocity;

    MotorControllerState():
      header(),
      joint_names_length(0), st_joint_names(), joint_names(nullptr),
      error_status_length(0), st_error_status(), error_status(nullptr),
      operational_state_length(0), st_operational_state(), operational_state(nullptr),
      motor_current_length(0), st_motor_current(), motor_current(nullptr),
      motor_voltage_length(0), st_motor_voltage(), motor_voltage(nullptr),
      absolute_position_iu_length(0), st_absolute_position_iu(), absolute_position_iu(nullptr),
      incremental_position_iu_length(0), st_incremental_position_iu(), incremental_position_iu(nullptr),
      absolute_velocity_iu_length(0), st_absolute_velocity_iu(), absolute_velocity_iu(nullptr),
      incremental_velocity_iu_length(0), st_incremental_velocity_iu(), incremental_velocity_iu(nullptr),
      absolute_position_length(0), st_absolute_position(), absolute_position(nullptr),
      incremental_position_length(0), st_incremental_position(), incremental_position(nullptr),
      absolute_velocity_length(0), st_absolute_velocity(), absolute_velocity(nullptr),
      incremental_velocity_length(0), st_incremental_velocity(), incremental_velocity(nullptr)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const override
    {
      int offset = 0;
      offset += this->header.serialize(outbuffer + offset);
      *(outbuffer + offset + 0) = (this->joint_names_length >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->joint_names_length >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->joint_names_length >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->joint_names_length >> (8 * 3)) & 0xFF;
      offset += sizeof(this->joint_names_length);
      for( uint32_t i = 0; i < joint_names_length; i++){
      uint32_t length_joint_namesi = strlen(this->joint_names[i]);
      varToArr(outbuffer + offset, length_joint_namesi);
      offset += 4;
      memcpy(outbuffer + offset, this->joint_names[i], length_joint_namesi);
      offset += length_joint_namesi;
      }
      *(outbuffer + offset + 0) = (this->error_status_length >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->error_status_length >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->error_status_length >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->error_status_length >> (8 * 3)) & 0xFF;
      offset += sizeof(this->error_status_length);
      for( uint32_t i = 0; i < error_status_length; i++){
      uint32_t length_error_statusi = strlen(this->error_status[i]);
      varToArr(outbuffer + offset, length_error_statusi);
      offset += 4;
      memcpy(outbuffer + offset, this->error_status[i], length_error_statusi);
      offset += length_error_statusi;
      }
      *(outbuffer + offset + 0) = (this->operational_state_length >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->operational_state_length >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->operational_state_length >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->operational_state_length >> (8 * 3)) & 0xFF;
      offset += sizeof(this->operational_state_length);
      for( uint32_t i = 0; i < operational_state_length; i++){
      uint32_t length_operational_statei = strlen(this->operational_state[i]);
      varToArr(outbuffer + offset, length_operational_statei);
      offset += 4;
      memcpy(outbuffer + offset, this->operational_state[i], length_operational_statei);
      offset += length_operational_statei;
      }
      *(outbuffer + offset + 0) = (this->motor_current_length >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->motor_current_length >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->motor_current_length >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->motor_current_length >> (8 * 3)) & 0xFF;
      offset += sizeof(this->motor_current_length);
      for( uint32_t i = 0; i < motor_current_length; i++){
      union {
        float real;
        uint32_t base;
      } u_motor_currenti;
      u_motor_currenti.real = this->motor_current[i];
      *(outbuffer + offset + 0) = (u_motor_currenti.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_motor_currenti.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_motor_currenti.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_motor_currenti.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->motor_current[i]);
      }
      *(outbuffer + offset + 0) = (this->motor_voltage_length >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->motor_voltage_length >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->motor_voltage_length >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->motor_voltage_length >> (8 * 3)) & 0xFF;
      offset += sizeof(this->motor_voltage_length);
      for( uint32_t i = 0; i < motor_voltage_length; i++){
      union {
        float real;
        uint32_t base;
      } u_motor_voltagei;
      u_motor_voltagei.real = this->motor_voltage[i];
      *(outbuffer + offset + 0) = (u_motor_voltagei.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_motor_voltagei.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_motor_voltagei.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_motor_voltagei.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->motor_voltage[i]);
      }
      *(outbuffer + offset + 0) = (this->absolute_position_iu_length >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->absolute_position_iu_length >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->absolute_position_iu_length >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->absolute_position_iu_length >> (8 * 3)) & 0xFF;
      offset += sizeof(this->absolute_position_iu_length);
      for( uint32_t i = 0; i < absolute_position_iu_length; i++){
      union {
        float real;
        uint32_t base;
      } u_absolute_position_iui;
      u_absolute_position_iui.real = this->absolute_position_iu[i];
      *(outbuffer + offset + 0) = (u_absolute_position_iui.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_absolute_position_iui.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_absolute_position_iui.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_absolute_position_iui.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->absolute_position_iu[i]);
      }
      *(outbuffer + offset + 0) = (this->incremental_position_iu_length >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->incremental_position_iu_length >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->incremental_position_iu_length >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->incremental_position_iu_length >> (8 * 3)) & 0xFF;
      offset += sizeof(this->incremental_position_iu_length);
      for( uint32_t i = 0; i < incremental_position_iu_length; i++){
      union {
        float real;
        uint32_t base;
      } u_incremental_position_iui;
      u_incremental_position_iui.real = this->incremental_position_iu[i];
      *(outbuffer + offset + 0) = (u_incremental_position_iui.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_incremental_position_iui.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_incremental_position_iui.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_incremental_position_iui.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->incremental_position_iu[i]);
      }
      *(outbuffer + offset + 0) = (this->absolute_velocity_iu_length >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->absolute_velocity_iu_length >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->absolute_velocity_iu_length >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->absolute_velocity_iu_length >> (8 * 3)) & 0xFF;
      offset += sizeof(this->absolute_velocity_iu_length);
      for( uint32_t i = 0; i < absolute_velocity_iu_length; i++){
      union {
        float real;
        uint32_t base;
      } u_absolute_velocity_iui;
      u_absolute_velocity_iui.real = this->absolute_velocity_iu[i];
      *(outbuffer + offset + 0) = (u_absolute_velocity_iui.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_absolute_velocity_iui.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_absolute_velocity_iui.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_absolute_velocity_iui.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->absolute_velocity_iu[i]);
      }
      *(outbuffer + offset + 0) = (this->incremental_velocity_iu_length >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->incremental_velocity_iu_length >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->incremental_velocity_iu_length >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->incremental_velocity_iu_length >> (8 * 3)) & 0xFF;
      offset += sizeof(this->incremental_velocity_iu_length);
      for( uint32_t i = 0; i < incremental_velocity_iu_length; i++){
      union {
        float real;
        uint32_t base;
      } u_incremental_velocity_iui;
      u_incremental_velocity_iui.real = this->incremental_velocity_iu[i];
      *(outbuffer + offset + 0) = (u_incremental_velocity_iui.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_incremental_velocity_iui.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_incremental_velocity_iui.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_incremental_velocity_iui.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->incremental_velocity_iu[i]);
      }
      *(outbuffer + offset + 0) = (this->absolute_position_length >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->absolute_position_length >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->absolute_position_length >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->absolute_position_length >> (8 * 3)) & 0xFF;
      offset += sizeof(this->absolute_position_length);
      for( uint32_t i = 0; i < absolute_position_length; i++){
      union {
        float real;
        uint32_t base;
      } u_absolute_positioni;
      u_absolute_positioni.real = this->absolute_position[i];
      *(outbuffer + offset + 0) = (u_absolute_positioni.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_absolute_positioni.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_absolute_positioni.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_absolute_positioni.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->absolute_position[i]);
      }
      *(outbuffer + offset + 0) = (this->incremental_position_length >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->incremental_position_length >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->incremental_position_length >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->incremental_position_length >> (8 * 3)) & 0xFF;
      offset += sizeof(this->incremental_position_length);
      for( uint32_t i = 0; i < incremental_position_length; i++){
      union {
        float real;
        uint32_t base;
      } u_incremental_positioni;
      u_incremental_positioni.real = this->incremental_position[i];
      *(outbuffer + offset + 0) = (u_incremental_positioni.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_incremental_positioni.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_incremental_positioni.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_incremental_positioni.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->incremental_position[i]);
      }
      *(outbuffer + offset + 0) = (this->absolute_velocity_length >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->absolute_velocity_length >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->absolute_velocity_length >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->absolute_velocity_length >> (8 * 3)) & 0xFF;
      offset += sizeof(this->absolute_velocity_length);
      for( uint32_t i = 0; i < absolute_velocity_length; i++){
      union {
        float real;
        uint32_t base;
      } u_absolute_velocityi;
      u_absolute_velocityi.real = this->absolute_velocity[i];
      *(outbuffer + offset + 0) = (u_absolute_velocityi.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_absolute_velocityi.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_absolute_velocityi.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_absolute_velocityi.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->absolute_velocity[i]);
      }
      *(outbuffer + offset + 0) = (this->incremental_velocity_length >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->incremental_velocity_length >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->incremental_velocity_length >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->incremental_velocity_length >> (8 * 3)) & 0xFF;
      offset += sizeof(this->incremental_velocity_length);
      for( uint32_t i = 0; i < incremental_velocity_length; i++){
      union {
        float real;
        uint32_t base;
      } u_incremental_velocityi;
      u_incremental_velocityi.real = this->incremental_velocity[i];
      *(outbuffer + offset + 0) = (u_incremental_velocityi.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_incremental_velocityi.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_incremental_velocityi.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_incremental_velocityi.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->incremental_velocity[i]);
      }
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer) override
    {
      int offset = 0;
      offset += this->header.deserialize(inbuffer + offset);
      uint32_t joint_names_lengthT = ((uint32_t) (*(inbuffer + offset))); 
      joint_names_lengthT |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1); 
      joint_names_lengthT |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2); 
      joint_names_lengthT |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3); 
      offset += sizeof(this->joint_names_length);
      if(joint_names_lengthT > joint_names_length)
        this->joint_names = (char**)realloc(this->joint_names, joint_names_lengthT * sizeof(char*));
      joint_names_length = joint_names_lengthT;
      for( uint32_t i = 0; i < joint_names_length; i++){
      uint32_t length_st_joint_names;
      arrToVar(length_st_joint_names, (inbuffer + offset));
      offset += 4;
      for(unsigned int k= offset; k< offset+length_st_joint_names; ++k){
          inbuffer[k-1]=inbuffer[k];
      }
      inbuffer[offset+length_st_joint_names-1]=0;
      this->st_joint_names = (char *)(inbuffer + offset-1);
      offset += length_st_joint_names;
        memcpy( &(this->joint_names[i]), &(this->st_joint_names), sizeof(char*));
      }
      uint32_t error_status_lengthT = ((uint32_t) (*(inbuffer + offset))); 
      error_status_lengthT |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1); 
      error_status_lengthT |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2); 
      error_status_lengthT |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3); 
      offset += sizeof(this->error_status_length);
      if(error_status_lengthT > error_status_length)
        this->error_status = (char**)realloc(this->error_status, error_status_lengthT * sizeof(char*));
      error_status_length = error_status_lengthT;
      for( uint32_t i = 0; i < error_status_length; i++){
      uint32_t length_st_error_status;
      arrToVar(length_st_error_status, (inbuffer + offset));
      offset += 4;
      for(unsigned int k= offset; k< offset+length_st_error_status; ++k){
          inbuffer[k-1]=inbuffer[k];
      }
      inbuffer[offset+length_st_error_status-1]=0;
      this->st_error_status = (char *)(inbuffer + offset-1);
      offset += length_st_error_status;
        memcpy( &(this->error_status[i]), &(this->st_error_status), sizeof(char*));
      }
      uint32_t operational_state_lengthT = ((uint32_t) (*(inbuffer + offset))); 
      operational_state_lengthT |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1); 
      operational_state_lengthT |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2); 
      operational_state_lengthT |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3); 
      offset += sizeof(this->operational_state_length);
      if(operational_state_lengthT > operational_state_length)
        this->operational_state = (char**)realloc(this->operational_state, operational_state_lengthT * sizeof(char*));
      operational_state_length = operational_state_lengthT;
      for( uint32_t i = 0; i < operational_state_length; i++){
      uint32_t length_st_operational_state;
      arrToVar(length_st_operational_state, (inbuffer + offset));
      offset += 4;
      for(unsigned int k= offset; k< offset+length_st_operational_state; ++k){
          inbuffer[k-1]=inbuffer[k];
      }
      inbuffer[offset+length_st_operational_state-1]=0;
      this->st_operational_state = (char *)(inbuffer + offset-1);
      offset += length_st_operational_state;
        memcpy( &(this->operational_state[i]), &(this->st_operational_state), sizeof(char*));
      }
      uint32_t motor_current_lengthT = ((uint32_t) (*(inbuffer + offset))); 
      motor_current_lengthT |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1); 
      motor_current_lengthT |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2); 
      motor_current_lengthT |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3); 
      offset += sizeof(this->motor_current_length);
      if(motor_current_lengthT > motor_current_length)
        this->motor_current = (float*)realloc(this->motor_current, motor_current_lengthT * sizeof(float));
      motor_current_length = motor_current_lengthT;
      for( uint32_t i = 0; i < motor_current_length; i++){
      union {
        float real;
        uint32_t base;
      } u_st_motor_current;
      u_st_motor_current.base = 0;
      u_st_motor_current.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_st_motor_current.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_st_motor_current.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_st_motor_current.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->st_motor_current = u_st_motor_current.real;
      offset += sizeof(this->st_motor_current);
        memcpy( &(this->motor_current[i]), &(this->st_motor_current), sizeof(float));
      }
      uint32_t motor_voltage_lengthT = ((uint32_t) (*(inbuffer + offset))); 
      motor_voltage_lengthT |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1); 
      motor_voltage_lengthT |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2); 
      motor_voltage_lengthT |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3); 
      offset += sizeof(this->motor_voltage_length);
      if(motor_voltage_lengthT > motor_voltage_length)
        this->motor_voltage = (float*)realloc(this->motor_voltage, motor_voltage_lengthT * sizeof(float));
      motor_voltage_length = motor_voltage_lengthT;
      for( uint32_t i = 0; i < motor_voltage_length; i++){
      union {
        float real;
        uint32_t base;
      } u_st_motor_voltage;
      u_st_motor_voltage.base = 0;
      u_st_motor_voltage.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_st_motor_voltage.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_st_motor_voltage.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_st_motor_voltage.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->st_motor_voltage = u_st_motor_voltage.real;
      offset += sizeof(this->st_motor_voltage);
        memcpy( &(this->motor_voltage[i]), &(this->st_motor_voltage), sizeof(float));
      }
      uint32_t absolute_position_iu_lengthT = ((uint32_t) (*(inbuffer + offset))); 
      absolute_position_iu_lengthT |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1); 
      absolute_position_iu_lengthT |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2); 
      absolute_position_iu_lengthT |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3); 
      offset += sizeof(this->absolute_position_iu_length);
      if(absolute_position_iu_lengthT > absolute_position_iu_length)
        this->absolute_position_iu = (float*)realloc(this->absolute_position_iu, absolute_position_iu_lengthT * sizeof(float));
      absolute_position_iu_length = absolute_position_iu_lengthT;
      for( uint32_t i = 0; i < absolute_position_iu_length; i++){
      union {
        float real;
        uint32_t base;
      } u_st_absolute_position_iu;
      u_st_absolute_position_iu.base = 0;
      u_st_absolute_position_iu.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_st_absolute_position_iu.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_st_absolute_position_iu.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_st_absolute_position_iu.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->st_absolute_position_iu = u_st_absolute_position_iu.real;
      offset += sizeof(this->st_absolute_position_iu);
        memcpy( &(this->absolute_position_iu[i]), &(this->st_absolute_position_iu), sizeof(float));
      }
      uint32_t incremental_position_iu_lengthT = ((uint32_t) (*(inbuffer + offset))); 
      incremental_position_iu_lengthT |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1); 
      incremental_position_iu_lengthT |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2); 
      incremental_position_iu_lengthT |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3); 
      offset += sizeof(this->incremental_position_iu_length);
      if(incremental_position_iu_lengthT > incremental_position_iu_length)
        this->incremental_position_iu = (float*)realloc(this->incremental_position_iu, incremental_position_iu_lengthT * sizeof(float));
      incremental_position_iu_length = incremental_position_iu_lengthT;
      for( uint32_t i = 0; i < incremental_position_iu_length; i++){
      union {
        float real;
        uint32_t base;
      } u_st_incremental_position_iu;
      u_st_incremental_position_iu.base = 0;
      u_st_incremental_position_iu.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_st_incremental_position_iu.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_st_incremental_position_iu.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_st_incremental_position_iu.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->st_incremental_position_iu = u_st_incremental_position_iu.real;
      offset += sizeof(this->st_incremental_position_iu);
        memcpy( &(this->incremental_position_iu[i]), &(this->st_incremental_position_iu), sizeof(float));
      }
      uint32_t absolute_velocity_iu_lengthT = ((uint32_t) (*(inbuffer + offset))); 
      absolute_velocity_iu_lengthT |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1); 
      absolute_velocity_iu_lengthT |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2); 
      absolute_velocity_iu_lengthT |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3); 
      offset += sizeof(this->absolute_velocity_iu_length);
      if(absolute_velocity_iu_lengthT > absolute_velocity_iu_length)
        this->absolute_velocity_iu = (float*)realloc(this->absolute_velocity_iu, absolute_velocity_iu_lengthT * sizeof(float));
      absolute_velocity_iu_length = absolute_velocity_iu_lengthT;
      for( uint32_t i = 0; i < absolute_velocity_iu_length; i++){
      union {
        float real;
        uint32_t base;
      } u_st_absolute_velocity_iu;
      u_st_absolute_velocity_iu.base = 0;
      u_st_absolute_velocity_iu.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_st_absolute_velocity_iu.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_st_absolute_velocity_iu.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_st_absolute_velocity_iu.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->st_absolute_velocity_iu = u_st_absolute_velocity_iu.real;
      offset += sizeof(this->st_absolute_velocity_iu);
        memcpy( &(this->absolute_velocity_iu[i]), &(this->st_absolute_velocity_iu), sizeof(float));
      }
      uint32_t incremental_velocity_iu_lengthT = ((uint32_t) (*(inbuffer + offset))); 
      incremental_velocity_iu_lengthT |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1); 
      incremental_velocity_iu_lengthT |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2); 
      incremental_velocity_iu_lengthT |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3); 
      offset += sizeof(this->incremental_velocity_iu_length);
      if(incremental_velocity_iu_lengthT > incremental_velocity_iu_length)
        this->incremental_velocity_iu = (float*)realloc(this->incremental_velocity_iu, incremental_velocity_iu_lengthT * sizeof(float));
      incremental_velocity_iu_length = incremental_velocity_iu_lengthT;
      for( uint32_t i = 0; i < incremental_velocity_iu_length; i++){
      union {
        float real;
        uint32_t base;
      } u_st_incremental_velocity_iu;
      u_st_incremental_velocity_iu.base = 0;
      u_st_incremental_velocity_iu.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_st_incremental_velocity_iu.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_st_incremental_velocity_iu.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_st_incremental_velocity_iu.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->st_incremental_velocity_iu = u_st_incremental_velocity_iu.real;
      offset += sizeof(this->st_incremental_velocity_iu);
        memcpy( &(this->incremental_velocity_iu[i]), &(this->st_incremental_velocity_iu), sizeof(float));
      }
      uint32_t absolute_position_lengthT = ((uint32_t) (*(inbuffer + offset))); 
      absolute_position_lengthT |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1); 
      absolute_position_lengthT |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2); 
      absolute_position_lengthT |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3); 
      offset += sizeof(this->absolute_position_length);
      if(absolute_position_lengthT > absolute_position_length)
        this->absolute_position = (float*)realloc(this->absolute_position, absolute_position_lengthT * sizeof(float));
      absolute_position_length = absolute_position_lengthT;
      for( uint32_t i = 0; i < absolute_position_length; i++){
      union {
        float real;
        uint32_t base;
      } u_st_absolute_position;
      u_st_absolute_position.base = 0;
      u_st_absolute_position.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_st_absolute_position.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_st_absolute_position.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_st_absolute_position.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->st_absolute_position = u_st_absolute_position.real;
      offset += sizeof(this->st_absolute_position);
        memcpy( &(this->absolute_position[i]), &(this->st_absolute_position), sizeof(float));
      }
      uint32_t incremental_position_lengthT = ((uint32_t) (*(inbuffer + offset))); 
      incremental_position_lengthT |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1); 
      incremental_position_lengthT |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2); 
      incremental_position_lengthT |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3); 
      offset += sizeof(this->incremental_position_length);
      if(incremental_position_lengthT > incremental_position_length)
        this->incremental_position = (float*)realloc(this->incremental_position, incremental_position_lengthT * sizeof(float));
      incremental_position_length = incremental_position_lengthT;
      for( uint32_t i = 0; i < incremental_position_length; i++){
      union {
        float real;
        uint32_t base;
      } u_st_incremental_position;
      u_st_incremental_position.base = 0;
      u_st_incremental_position.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_st_incremental_position.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_st_incremental_position.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_st_incremental_position.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->st_incremental_position = u_st_incremental_position.real;
      offset += sizeof(this->st_incremental_position);
        memcpy( &(this->incremental_position[i]), &(this->st_incremental_position), sizeof(float));
      }
      uint32_t absolute_velocity_lengthT = ((uint32_t) (*(inbuffer + offset))); 
      absolute_velocity_lengthT |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1); 
      absolute_velocity_lengthT |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2); 
      absolute_velocity_lengthT |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3); 
      offset += sizeof(this->absolute_velocity_length);
      if(absolute_velocity_lengthT > absolute_velocity_length)
        this->absolute_velocity = (float*)realloc(this->absolute_velocity, absolute_velocity_lengthT * sizeof(float));
      absolute_velocity_length = absolute_velocity_lengthT;
      for( uint32_t i = 0; i < absolute_velocity_length; i++){
      union {
        float real;
        uint32_t base;
      } u_st_absolute_velocity;
      u_st_absolute_velocity.base = 0;
      u_st_absolute_velocity.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_st_absolute_velocity.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_st_absolute_velocity.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_st_absolute_velocity.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->st_absolute_velocity = u_st_absolute_velocity.real;
      offset += sizeof(this->st_absolute_velocity);
        memcpy( &(this->absolute_velocity[i]), &(this->st_absolute_velocity), sizeof(float));
      }
      uint32_t incremental_velocity_lengthT = ((uint32_t) (*(inbuffer + offset))); 
      incremental_velocity_lengthT |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1); 
      incremental_velocity_lengthT |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2); 
      incremental_velocity_lengthT |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3); 
      offset += sizeof(this->incremental_velocity_length);
      if(incremental_velocity_lengthT > incremental_velocity_length)
        this->incremental_velocity = (float*)realloc(this->incremental_velocity, incremental_velocity_lengthT * sizeof(float));
      incremental_velocity_length = incremental_velocity_lengthT;
      for( uint32_t i = 0; i < incremental_velocity_length; i++){
      union {
        float real;
        uint32_t base;
      } u_st_incremental_velocity;
      u_st_incremental_velocity.base = 0;
      u_st_incremental_velocity.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_st_incremental_velocity.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_st_incremental_velocity.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_st_incremental_velocity.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->st_incremental_velocity = u_st_incremental_velocity.real;
      offset += sizeof(this->st_incremental_velocity);
        memcpy( &(this->incremental_velocity[i]), &(this->st_incremental_velocity), sizeof(float));
      }
     return offset;
    }

    virtual const char * getType() override { return "march_shared_msgs/MotorControllerState"; };
    virtual const char * getMD5() override { return "faf75ae27a1d2b4807f64e5694585fab"; };

  };

}
#endif
