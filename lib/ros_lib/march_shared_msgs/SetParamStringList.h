#ifndef _ROS_SERVICE_SetParamStringList_h
#define _ROS_SERVICE_SetParamStringList_h
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace march_shared_msgs
{

static const char SETPARAMSTRINGLIST[] = "march_shared_msgs/SetParamStringList";

  class SetParamStringListRequest : public ros::Msg
  {
    public:
      typedef const char* _name_type;
      _name_type name;
      uint32_t value_length;
      typedef char* _value_type;
      _value_type st_value;
      _value_type * value;

    SetParamStringListRequest():
      name(""),
      value_length(0), st_value(), value(nullptr)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const override
    {
      int offset = 0;
      uint32_t length_name = strlen(this->name);
      varToArr(outbuffer + offset, length_name);
      offset += 4;
      memcpy(outbuffer + offset, this->name, length_name);
      offset += length_name;
      *(outbuffer + offset + 0) = (this->value_length >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->value_length >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->value_length >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->value_length >> (8 * 3)) & 0xFF;
      offset += sizeof(this->value_length);
      for( uint32_t i = 0; i < value_length; i++){
      uint32_t length_valuei = strlen(this->value[i]);
      varToArr(outbuffer + offset, length_valuei);
      offset += 4;
      memcpy(outbuffer + offset, this->value[i], length_valuei);
      offset += length_valuei;
      }
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer) override
    {
      int offset = 0;
      uint32_t length_name;
      arrToVar(length_name, (inbuffer + offset));
      offset += 4;
      for(unsigned int k= offset; k< offset+length_name; ++k){
          inbuffer[k-1]=inbuffer[k];
      }
      inbuffer[offset+length_name-1]=0;
      this->name = (char *)(inbuffer + offset-1);
      offset += length_name;
      uint32_t value_lengthT = ((uint32_t) (*(inbuffer + offset))); 
      value_lengthT |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1); 
      value_lengthT |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2); 
      value_lengthT |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3); 
      offset += sizeof(this->value_length);
      if(value_lengthT > value_length)
        this->value = (char**)realloc(this->value, value_lengthT * sizeof(char*));
      value_length = value_lengthT;
      for( uint32_t i = 0; i < value_length; i++){
      uint32_t length_st_value;
      arrToVar(length_st_value, (inbuffer + offset));
      offset += 4;
      for(unsigned int k= offset; k< offset+length_st_value; ++k){
          inbuffer[k-1]=inbuffer[k];
      }
      inbuffer[offset+length_st_value-1]=0;
      this->st_value = (char *)(inbuffer + offset-1);
      offset += length_st_value;
        memcpy( &(this->value[i]), &(this->st_value), sizeof(char*));
      }
     return offset;
    }

    virtual const char * getType() override { return SETPARAMSTRINGLIST; };
    virtual const char * getMD5() override { return "a20c942da620dbf29654f430aa251492"; };

  };

  class SetParamStringListResponse : public ros::Msg
  {
    public:
      typedef bool _success_type;
      _success_type success;

    SetParamStringListResponse():
      success(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const override
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_success;
      u_success.real = this->success;
      *(outbuffer + offset + 0) = (u_success.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->success);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer) override
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_success;
      u_success.base = 0;
      u_success.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->success = u_success.real;
      offset += sizeof(this->success);
     return offset;
    }

    virtual const char * getType() override { return SETPARAMSTRINGLIST; };
    virtual const char * getMD5() override { return "358e233cde0c8a8bcfea4ce193f8fc15"; };

  };

  class SetParamStringList {
    public:
    typedef SetParamStringListRequest Request;
    typedef SetParamStringListResponse Response;
  };

}
#endif
