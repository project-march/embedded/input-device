#ifndef _ROS_march_shared_msgs_GaitParameters_h
#define _ROS_march_shared_msgs_GaitParameters_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace march_shared_msgs
{

  class GaitParameters : public ros::Msg
  {
    public:
      typedef float _first_parameter_type;
      _first_parameter_type first_parameter;
      typedef float _second_parameter_type;
      _second_parameter_type second_parameter;
      typedef float _side_step_parameter_type;
      _side_step_parameter_type side_step_parameter;

    GaitParameters():
      first_parameter(0),
      second_parameter(0),
      side_step_parameter(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const override
    {
      int offset = 0;
      offset += serializeAvrFloat64(outbuffer + offset, this->first_parameter);
      offset += serializeAvrFloat64(outbuffer + offset, this->second_parameter);
      offset += serializeAvrFloat64(outbuffer + offset, this->side_step_parameter);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer) override
    {
      int offset = 0;
      offset += deserializeAvrFloat64(inbuffer + offset, &(this->first_parameter));
      offset += deserializeAvrFloat64(inbuffer + offset, &(this->second_parameter));
      offset += deserializeAvrFloat64(inbuffer + offset, &(this->side_step_parameter));
     return offset;
    }

    virtual const char * getType() override { return "march_shared_msgs/GaitParameters"; };
    virtual const char * getMD5() override { return "58a366a4dcd5d62c1dd91cb2831fcb86"; };

  };

}
#endif
