#ifndef _ROS_SERVICE_CapturePointPose_h
#define _ROS_SERVICE_CapturePointPose_h
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "geometry_msgs/Pose.h"

namespace march_shared_msgs
{

static const char CAPTUREPOINTPOSE[] = "march_shared_msgs/CapturePointPose";

  class CapturePointPoseRequest : public ros::Msg
  {
    public:
      typedef float _duration_type;
      _duration_type duration;

    CapturePointPoseRequest():
      duration(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const override
    {
      int offset = 0;
      offset += serializeAvrFloat64(outbuffer + offset, this->duration);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer) override
    {
      int offset = 0;
      offset += deserializeAvrFloat64(inbuffer + offset, &(this->duration));
     return offset;
    }

    virtual const char * getType() override { return CAPTUREPOINTPOSE; };
    virtual const char * getMD5() override { return "efce4fd63555f8abdd034fb55c87e5aa"; };

  };

  class CapturePointPoseResponse : public ros::Msg
  {
    public:
      typedef bool _success_type;
      _success_type success;
      typedef float _duration_type;
      _duration_type duration;
      typedef geometry_msgs::Pose _capture_point_type;
      _capture_point_type capture_point;

    CapturePointPoseResponse():
      success(0),
      duration(0),
      capture_point()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const override
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_success;
      u_success.real = this->success;
      *(outbuffer + offset + 0) = (u_success.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->success);
      offset += serializeAvrFloat64(outbuffer + offset, this->duration);
      offset += this->capture_point.serialize(outbuffer + offset);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer) override
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_success;
      u_success.base = 0;
      u_success.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->success = u_success.real;
      offset += sizeof(this->success);
      offset += deserializeAvrFloat64(inbuffer + offset, &(this->duration));
      offset += this->capture_point.deserialize(inbuffer + offset);
     return offset;
    }

    virtual const char * getType() override { return CAPTUREPOINTPOSE; };
    virtual const char * getMD5() override { return "5b61318011b94ca2af6e372bb1921706"; };

  };

  class CapturePointPose {
    public:
    typedef CapturePointPoseRequest Request;
    typedef CapturePointPoseResponse Response;
  };

}
#endif
