#ifndef _ROS_SERVICE_GetGaitParameters_h
#define _ROS_SERVICE_GetGaitParameters_h
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "march_shared_msgs/GaitParameters.h"

namespace march_shared_msgs
{

static const char GETGAITPARAMETERS[] = "march_shared_msgs/GetGaitParameters";

  class GetGaitParametersRequest : public ros::Msg
  {
    public:
      typedef uint8_t _realsense_category_type;
      _realsense_category_type realsense_category;
      typedef const char* _frame_id_to_transform_to_type;
      _frame_id_to_transform_to_type frame_id_to_transform_to;
      typedef uint8_t _camera_to_use_type;
      _camera_to_use_type camera_to_use;
      enum { STAIRS_UP = 0 };
      enum { STAIRS_DOWN = 1 };
      enum { RAMP_UP = 2 };
      enum { RAMP_DOWN = 3 };
      enum { SIT = 4 };
      enum { CAMERA_FRONT = 0 };
      enum { CAMERA_BACK = 1 };

    GetGaitParametersRequest():
      realsense_category(0),
      frame_id_to_transform_to(""),
      camera_to_use(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const override
    {
      int offset = 0;
      *(outbuffer + offset + 0) = (this->realsense_category >> (8 * 0)) & 0xFF;
      offset += sizeof(this->realsense_category);
      uint32_t length_frame_id_to_transform_to = strlen(this->frame_id_to_transform_to);
      varToArr(outbuffer + offset, length_frame_id_to_transform_to);
      offset += 4;
      memcpy(outbuffer + offset, this->frame_id_to_transform_to, length_frame_id_to_transform_to);
      offset += length_frame_id_to_transform_to;
      *(outbuffer + offset + 0) = (this->camera_to_use >> (8 * 0)) & 0xFF;
      offset += sizeof(this->camera_to_use);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer) override
    {
      int offset = 0;
      this->realsense_category =  ((uint8_t) (*(inbuffer + offset)));
      offset += sizeof(this->realsense_category);
      uint32_t length_frame_id_to_transform_to;
      arrToVar(length_frame_id_to_transform_to, (inbuffer + offset));
      offset += 4;
      for(unsigned int k= offset; k< offset+length_frame_id_to_transform_to; ++k){
          inbuffer[k-1]=inbuffer[k];
      }
      inbuffer[offset+length_frame_id_to_transform_to-1]=0;
      this->frame_id_to_transform_to = (char *)(inbuffer + offset-1);
      offset += length_frame_id_to_transform_to;
      this->camera_to_use =  ((uint8_t) (*(inbuffer + offset)));
      offset += sizeof(this->camera_to_use);
     return offset;
    }

    virtual const char * getType() override { return GETGAITPARAMETERS; };
    virtual const char * getMD5() override { return "3d4b5ebbdf46cd91e72f304f050e43bf"; };

  };

  class GetGaitParametersResponse : public ros::Msg
  {
    public:
      typedef bool _success_type;
      _success_type success;
      typedef const char* _error_message_type;
      _error_message_type error_message;
      typedef march_shared_msgs::GaitParameters _gait_parameters_type;
      _gait_parameters_type gait_parameters;

    GetGaitParametersResponse():
      success(0),
      error_message(""),
      gait_parameters()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const override
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_success;
      u_success.real = this->success;
      *(outbuffer + offset + 0) = (u_success.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->success);
      uint32_t length_error_message = strlen(this->error_message);
      varToArr(outbuffer + offset, length_error_message);
      offset += 4;
      memcpy(outbuffer + offset, this->error_message, length_error_message);
      offset += length_error_message;
      offset += this->gait_parameters.serialize(outbuffer + offset);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer) override
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_success;
      u_success.base = 0;
      u_success.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->success = u_success.real;
      offset += sizeof(this->success);
      uint32_t length_error_message;
      arrToVar(length_error_message, (inbuffer + offset));
      offset += 4;
      for(unsigned int k= offset; k< offset+length_error_message; ++k){
          inbuffer[k-1]=inbuffer[k];
      }
      inbuffer[offset+length_error_message-1]=0;
      this->error_message = (char *)(inbuffer + offset-1);
      offset += length_error_message;
      offset += this->gait_parameters.deserialize(inbuffer + offset);
     return offset;
    }

    virtual const char * getType() override { return GETGAITPARAMETERS; };
    virtual const char * getMD5() override { return "74aa6951bcb473ae663b5dd3a7257c22"; };

  };

  class GetGaitParameters {
    public:
    typedef GetGaitParametersRequest Request;
    typedef GetGaitParametersResponse Response;
  };

}
#endif
