#ifndef _ROS_march_shared_msgs_PressureSoleData_h
#define _ROS_march_shared_msgs_PressureSoleData_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace march_shared_msgs
{

  class PressureSoleData : public ros::Msg
  {
    public:
      typedef const char* _side_type;
      _side_type side;
      typedef float _heel_right_type;
      _heel_right_type heel_right;
      typedef float _heel_left_type;
      _heel_left_type heel_left;
      typedef float _met1_type;
      _met1_type met1;
      typedef float _hallux_type;
      _hallux_type hallux;
      typedef float _met3_type;
      _met3_type met3;
      typedef float _toes_type;
      _toes_type toes;
      typedef float _met5_type;
      _met5_type met5;
      typedef float _arch_type;
      _arch_type arch;

    PressureSoleData():
      side(""),
      heel_right(0),
      heel_left(0),
      met1(0),
      hallux(0),
      met3(0),
      toes(0),
      met5(0),
      arch(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const override
    {
      int offset = 0;
      uint32_t length_side = strlen(this->side);
      varToArr(outbuffer + offset, length_side);
      offset += 4;
      memcpy(outbuffer + offset, this->side, length_side);
      offset += length_side;
      union {
        float real;
        uint32_t base;
      } u_heel_right;
      u_heel_right.real = this->heel_right;
      *(outbuffer + offset + 0) = (u_heel_right.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_heel_right.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_heel_right.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_heel_right.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->heel_right);
      union {
        float real;
        uint32_t base;
      } u_heel_left;
      u_heel_left.real = this->heel_left;
      *(outbuffer + offset + 0) = (u_heel_left.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_heel_left.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_heel_left.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_heel_left.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->heel_left);
      union {
        float real;
        uint32_t base;
      } u_met1;
      u_met1.real = this->met1;
      *(outbuffer + offset + 0) = (u_met1.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_met1.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_met1.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_met1.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->met1);
      union {
        float real;
        uint32_t base;
      } u_hallux;
      u_hallux.real = this->hallux;
      *(outbuffer + offset + 0) = (u_hallux.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_hallux.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_hallux.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_hallux.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->hallux);
      union {
        float real;
        uint32_t base;
      } u_met3;
      u_met3.real = this->met3;
      *(outbuffer + offset + 0) = (u_met3.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_met3.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_met3.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_met3.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->met3);
      union {
        float real;
        uint32_t base;
      } u_toes;
      u_toes.real = this->toes;
      *(outbuffer + offset + 0) = (u_toes.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_toes.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_toes.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_toes.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->toes);
      union {
        float real;
        uint32_t base;
      } u_met5;
      u_met5.real = this->met5;
      *(outbuffer + offset + 0) = (u_met5.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_met5.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_met5.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_met5.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->met5);
      union {
        float real;
        uint32_t base;
      } u_arch;
      u_arch.real = this->arch;
      *(outbuffer + offset + 0) = (u_arch.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_arch.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_arch.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_arch.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->arch);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer) override
    {
      int offset = 0;
      uint32_t length_side;
      arrToVar(length_side, (inbuffer + offset));
      offset += 4;
      for(unsigned int k= offset; k< offset+length_side; ++k){
          inbuffer[k-1]=inbuffer[k];
      }
      inbuffer[offset+length_side-1]=0;
      this->side = (char *)(inbuffer + offset-1);
      offset += length_side;
      union {
        float real;
        uint32_t base;
      } u_heel_right;
      u_heel_right.base = 0;
      u_heel_right.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_heel_right.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_heel_right.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_heel_right.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->heel_right = u_heel_right.real;
      offset += sizeof(this->heel_right);
      union {
        float real;
        uint32_t base;
      } u_heel_left;
      u_heel_left.base = 0;
      u_heel_left.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_heel_left.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_heel_left.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_heel_left.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->heel_left = u_heel_left.real;
      offset += sizeof(this->heel_left);
      union {
        float real;
        uint32_t base;
      } u_met1;
      u_met1.base = 0;
      u_met1.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_met1.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_met1.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_met1.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->met1 = u_met1.real;
      offset += sizeof(this->met1);
      union {
        float real;
        uint32_t base;
      } u_hallux;
      u_hallux.base = 0;
      u_hallux.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_hallux.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_hallux.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_hallux.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->hallux = u_hallux.real;
      offset += sizeof(this->hallux);
      union {
        float real;
        uint32_t base;
      } u_met3;
      u_met3.base = 0;
      u_met3.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_met3.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_met3.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_met3.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->met3 = u_met3.real;
      offset += sizeof(this->met3);
      union {
        float real;
        uint32_t base;
      } u_toes;
      u_toes.base = 0;
      u_toes.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_toes.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_toes.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_toes.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->toes = u_toes.real;
      offset += sizeof(this->toes);
      union {
        float real;
        uint32_t base;
      } u_met5;
      u_met5.base = 0;
      u_met5.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_met5.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_met5.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_met5.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->met5 = u_met5.real;
      offset += sizeof(this->met5);
      union {
        float real;
        uint32_t base;
      } u_arch;
      u_arch.base = 0;
      u_arch.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_arch.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_arch.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_arch.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->arch = u_arch.real;
      offset += sizeof(this->arch);
     return offset;
    }

    virtual const char * getType() override { return "march_shared_msgs/PressureSoleData"; };
    virtual const char * getMD5() override { return "f778afef2a62daf4a25bbc0aa80cca01"; };

  };

}
#endif
