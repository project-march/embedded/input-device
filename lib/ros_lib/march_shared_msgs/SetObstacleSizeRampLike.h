#ifndef _ROS_SERVICE_SetObstacleSizeRampLike_h
#define _ROS_SERVICE_SetObstacleSizeRampLike_h
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace march_shared_msgs
{

static const char SETOBSTACLESIZERAMPLIKE[] = "march_shared_msgs/SetObstacleSizeRampLike";

  class SetObstacleSizeRampLikeRequest : public ros::Msg
  {
    public:
      typedef const char* _obstacle_name_type;
      _obstacle_name_type obstacle_name;
      typedef float _new_slope_up_type;
      _new_slope_up_type new_slope_up;
      typedef float _new_slope_down_type;
      _new_slope_down_type new_slope_down;
      typedef float _new_ramp_up_length_type;
      _new_ramp_up_length_type new_ramp_up_length;

    SetObstacleSizeRampLikeRequest():
      obstacle_name(""),
      new_slope_up(0),
      new_slope_down(0),
      new_ramp_up_length(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const override
    {
      int offset = 0;
      uint32_t length_obstacle_name = strlen(this->obstacle_name);
      varToArr(outbuffer + offset, length_obstacle_name);
      offset += 4;
      memcpy(outbuffer + offset, this->obstacle_name, length_obstacle_name);
      offset += length_obstacle_name;
      offset += serializeAvrFloat64(outbuffer + offset, this->new_slope_up);
      offset += serializeAvrFloat64(outbuffer + offset, this->new_slope_down);
      offset += serializeAvrFloat64(outbuffer + offset, this->new_ramp_up_length);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer) override
    {
      int offset = 0;
      uint32_t length_obstacle_name;
      arrToVar(length_obstacle_name, (inbuffer + offset));
      offset += 4;
      for(unsigned int k= offset; k< offset+length_obstacle_name; ++k){
          inbuffer[k-1]=inbuffer[k];
      }
      inbuffer[offset+length_obstacle_name-1]=0;
      this->obstacle_name = (char *)(inbuffer + offset-1);
      offset += length_obstacle_name;
      offset += deserializeAvrFloat64(inbuffer + offset, &(this->new_slope_up));
      offset += deserializeAvrFloat64(inbuffer + offset, &(this->new_slope_down));
      offset += deserializeAvrFloat64(inbuffer + offset, &(this->new_ramp_up_length));
     return offset;
    }

    virtual const char * getType() override { return SETOBSTACLESIZERAMPLIKE; };
    virtual const char * getMD5() override { return "1ea9952cd7bffd0375af80cea7910020"; };

  };

  class SetObstacleSizeRampLikeResponse : public ros::Msg
  {
    public:
      typedef bool _success_type;
      _success_type success;

    SetObstacleSizeRampLikeResponse():
      success(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const override
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_success;
      u_success.real = this->success;
      *(outbuffer + offset + 0) = (u_success.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->success);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer) override
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_success;
      u_success.base = 0;
      u_success.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->success = u_success.real;
      offset += sizeof(this->success);
     return offset;
    }

    virtual const char * getType() override { return SETOBSTACLESIZERAMPLIKE; };
    virtual const char * getMD5() override { return "358e233cde0c8a8bcfea4ce193f8fc15"; };

  };

  class SetObstacleSizeRampLike {
    public:
    typedef SetObstacleSizeRampLikeRequest Request;
    typedef SetObstacleSizeRampLikeResponse Response;
  };

}
#endif
