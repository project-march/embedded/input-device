#ifndef _ROS_SERVICE_SetObstacleSizeBlockLike_h
#define _ROS_SERVICE_SetObstacleSizeBlockLike_h
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace march_shared_msgs
{

static const char SETOBSTACLESIZEBLOCKLIKE[] = "march_shared_msgs/SetObstacleSizeBlockLike";

  class SetObstacleSizeBlockLikeRequest : public ros::Msg
  {
    public:
      typedef const char* _obstacle_name_type;
      _obstacle_name_type obstacle_name;
      typedef float _new_height_type;
      _new_height_type new_height;
      typedef float _new_width_type;
      _new_width_type new_width;
      typedef float _new_length_type;
      _new_length_type new_length;

    SetObstacleSizeBlockLikeRequest():
      obstacle_name(""),
      new_height(0),
      new_width(0),
      new_length(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const override
    {
      int offset = 0;
      uint32_t length_obstacle_name = strlen(this->obstacle_name);
      varToArr(outbuffer + offset, length_obstacle_name);
      offset += 4;
      memcpy(outbuffer + offset, this->obstacle_name, length_obstacle_name);
      offset += length_obstacle_name;
      offset += serializeAvrFloat64(outbuffer + offset, this->new_height);
      offset += serializeAvrFloat64(outbuffer + offset, this->new_width);
      offset += serializeAvrFloat64(outbuffer + offset, this->new_length);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer) override
    {
      int offset = 0;
      uint32_t length_obstacle_name;
      arrToVar(length_obstacle_name, (inbuffer + offset));
      offset += 4;
      for(unsigned int k= offset; k< offset+length_obstacle_name; ++k){
          inbuffer[k-1]=inbuffer[k];
      }
      inbuffer[offset+length_obstacle_name-1]=0;
      this->obstacle_name = (char *)(inbuffer + offset-1);
      offset += length_obstacle_name;
      offset += deserializeAvrFloat64(inbuffer + offset, &(this->new_height));
      offset += deserializeAvrFloat64(inbuffer + offset, &(this->new_width));
      offset += deserializeAvrFloat64(inbuffer + offset, &(this->new_length));
     return offset;
    }

    virtual const char * getType() override { return SETOBSTACLESIZEBLOCKLIKE; };
    virtual const char * getMD5() override { return "4a35ae4ec69809438e13b3a689cf430f"; };

  };

  class SetObstacleSizeBlockLikeResponse : public ros::Msg
  {
    public:
      typedef bool _success_type;
      _success_type success;

    SetObstacleSizeBlockLikeResponse():
      success(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const override
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_success;
      u_success.real = this->success;
      *(outbuffer + offset + 0) = (u_success.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->success);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer) override
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_success;
      u_success.base = 0;
      u_success.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->success = u_success.real;
      offset += sizeof(this->success);
     return offset;
    }

    virtual const char * getType() override { return SETOBSTACLESIZEBLOCKLIKE; };
    virtual const char * getMD5() override { return "358e233cde0c8a8bcfea4ce193f8fc15"; };

  };

  class SetObstacleSizeBlockLike {
    public:
    typedef SetObstacleSizeBlockLikeRequest Request;
    typedef SetObstacleSizeBlockLikeResponse Response;
  };

}
#endif
