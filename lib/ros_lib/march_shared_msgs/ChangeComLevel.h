#ifndef _ROS_SERVICE_ChangeComLevel_h
#define _ROS_SERVICE_ChangeComLevel_h
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace march_shared_msgs
{

static const char CHANGECOMLEVEL[] = "march_shared_msgs/ChangeComLevel";

  class ChangeComLevelRequest : public ros::Msg
  {
    public:
      typedef const char* _level_name_type;
      _level_name_type level_name;

    ChangeComLevelRequest():
      level_name("")
    {
    }

    virtual int serialize(unsigned char *outbuffer) const override
    {
      int offset = 0;
      uint32_t length_level_name = strlen(this->level_name);
      varToArr(outbuffer + offset, length_level_name);
      offset += 4;
      memcpy(outbuffer + offset, this->level_name, length_level_name);
      offset += length_level_name;
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer) override
    {
      int offset = 0;
      uint32_t length_level_name;
      arrToVar(length_level_name, (inbuffer + offset));
      offset += 4;
      for(unsigned int k= offset; k< offset+length_level_name; ++k){
          inbuffer[k-1]=inbuffer[k];
      }
      inbuffer[offset+length_level_name-1]=0;
      this->level_name = (char *)(inbuffer + offset-1);
      offset += length_level_name;
     return offset;
    }

    virtual const char * getType() override { return CHANGECOMLEVEL; };
    virtual const char * getMD5() override { return "c4b9dd4307396b5ed78a70944f143970"; };

  };

  class ChangeComLevelResponse : public ros::Msg
  {
    public:
      typedef bool _success_type;
      _success_type success;

    ChangeComLevelResponse():
      success(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const override
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_success;
      u_success.real = this->success;
      *(outbuffer + offset + 0) = (u_success.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->success);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer) override
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_success;
      u_success.base = 0;
      u_success.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->success = u_success.real;
      offset += sizeof(this->success);
     return offset;
    }

    virtual const char * getType() override { return CHANGECOMLEVEL; };
    virtual const char * getMD5() override { return "358e233cde0c8a8bcfea4ce193f8fc15"; };

  };

  class ChangeComLevel {
    public:
    typedef ChangeComLevelRequest Request;
    typedef ChangeComLevelResponse Response;
  };

}
#endif
