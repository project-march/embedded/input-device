#ifndef _ROS_march_shared_msgs_PressureSolesData_h
#define _ROS_march_shared_msgs_PressureSolesData_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "std_msgs/Header.h"
#include "march_shared_msgs/PressureSoleData.h"

namespace march_shared_msgs
{

  class PressureSolesData : public ros::Msg
  {
    public:
      typedef std_msgs::Header _header_type;
      _header_type header;
      typedef march_shared_msgs::PressureSoleData _left_foot_type;
      _left_foot_type left_foot;
      typedef march_shared_msgs::PressureSoleData _right_foot_type;
      _right_foot_type right_foot;

    PressureSolesData():
      header(),
      left_foot(),
      right_foot()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const override
    {
      int offset = 0;
      offset += this->header.serialize(outbuffer + offset);
      offset += this->left_foot.serialize(outbuffer + offset);
      offset += this->right_foot.serialize(outbuffer + offset);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer) override
    {
      int offset = 0;
      offset += this->header.deserialize(inbuffer + offset);
      offset += this->left_foot.deserialize(inbuffer + offset);
      offset += this->right_foot.deserialize(inbuffer + offset);
     return offset;
    }

    virtual const char * getType() override { return "march_shared_msgs/PressureSolesData"; };
    virtual const char * getMD5() override { return "61e9455c83eb07e8d58853889650c446"; };

  };

}
#endif
