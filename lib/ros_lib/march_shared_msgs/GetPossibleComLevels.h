#ifndef _ROS_SERVICE_GetPossibleComLevels_h
#define _ROS_SERVICE_GetPossibleComLevels_h
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace march_shared_msgs
{

static const char GETPOSSIBLECOMLEVELS[] = "march_shared_msgs/GetPossibleComLevels";

  class GetPossibleComLevelsRequest : public ros::Msg
  {
    public:

    GetPossibleComLevelsRequest()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const override
    {
      int offset = 0;
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer) override
    {
      int offset = 0;
     return offset;
    }

    virtual const char * getType() override { return GETPOSSIBLECOMLEVELS; };
    virtual const char * getMD5() override { return "d41d8cd98f00b204e9800998ecf8427e"; };

  };

  class GetPossibleComLevelsResponse : public ros::Msg
  {
    public:
      uint32_t com_levels_length;
      typedef char* _com_levels_type;
      _com_levels_type st_com_levels;
      _com_levels_type * com_levels;

    GetPossibleComLevelsResponse():
      com_levels_length(0), st_com_levels(), com_levels(nullptr)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const override
    {
      int offset = 0;
      *(outbuffer + offset + 0) = (this->com_levels_length >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->com_levels_length >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->com_levels_length >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->com_levels_length >> (8 * 3)) & 0xFF;
      offset += sizeof(this->com_levels_length);
      for( uint32_t i = 0; i < com_levels_length; i++){
      uint32_t length_com_levelsi = strlen(this->com_levels[i]);
      varToArr(outbuffer + offset, length_com_levelsi);
      offset += 4;
      memcpy(outbuffer + offset, this->com_levels[i], length_com_levelsi);
      offset += length_com_levelsi;
      }
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer) override
    {
      int offset = 0;
      uint32_t com_levels_lengthT = ((uint32_t) (*(inbuffer + offset))); 
      com_levels_lengthT |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1); 
      com_levels_lengthT |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2); 
      com_levels_lengthT |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3); 
      offset += sizeof(this->com_levels_length);
      if(com_levels_lengthT > com_levels_length)
        this->com_levels = (char**)realloc(this->com_levels, com_levels_lengthT * sizeof(char*));
      com_levels_length = com_levels_lengthT;
      for( uint32_t i = 0; i < com_levels_length; i++){
      uint32_t length_st_com_levels;
      arrToVar(length_st_com_levels, (inbuffer + offset));
      offset += 4;
      for(unsigned int k= offset; k< offset+length_st_com_levels; ++k){
          inbuffer[k-1]=inbuffer[k];
      }
      inbuffer[offset+length_st_com_levels-1]=0;
      this->st_com_levels = (char *)(inbuffer + offset-1);
      offset += length_st_com_levels;
        memcpy( &(this->com_levels[i]), &(this->st_com_levels), sizeof(char*));
      }
     return offset;
    }

    virtual const char * getType() override { return GETPOSSIBLECOMLEVELS; };
    virtual const char * getMD5() override { return "d3788254bb551de66e95588b3a42b6ba"; };

  };

  class GetPossibleComLevels {
    public:
    typedef GetPossibleComLevelsRequest Request;
    typedef GetPossibleComLevelsResponse Response;
  };

}
#endif
