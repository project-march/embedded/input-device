#include "state_machine.h"
#include "states_array.h"
#include "gait_sequence.h"
#include "state_machine_construction.h"

const std::string StateMachine::get_current_gait_name() const {
  return current_state_->get_gait_name();
}

const char* StateMachine::get_current_state_name() const {
  return current_state_->get_raw_state_name();
}

const SectorAddress& StateMachine::get_current_image() const {
  return current_state_->get_image();
}

const size_t StateMachine::size() const {
  return states_array.size();
}

bool StateMachine::left() {
  return has_state() && set_current_state(current_state_->get_left());
}

bool StateMachine::right() {
  return has_state() && set_current_state(current_state_->get_right());
}

bool StateMachine::shortcut_push() {
  if(!shortcut_states.empty())
  {
    if(!current_state_->get_in_shortcut_menu()){
      for(int i=0; i<shortcut_states.size(); ++i){
        shortcut_states.at(i)->set_back(current_state_);
      }
      return has_state() && set_current_state(current_state_->get_shortcut_push());
    }
  }
  return false;
}

bool StateMachine::back() {
  return has_state() && set_current_state(current_state_->get_back());
}

bool StateMachine::select() {
  return has_state() && set_current_state(current_state_->get_select());
}

bool StateMachine::activate() {
  return has_state() && set_current_state(current_state_->get_activate());
}

bool StateMachine::has_state() const {
  return current_state_ != nullptr;
}

bool StateMachine::set_current_state(const State* new_state) {
  bool has_changed = current_state_ != new_state;
  current_state_ = new_state;
  return has_changed;
}

void StateMachine::construct() {
  ShortcutMenu shortcut_menu = construct_statemachine();
  set_current_state(&states_array.at(TRAINING_START_MENU_HOME_SIT));
  shortcut_states.push_back(shortcut_menu.get_state_first());
  shortcut_states.push_back(shortcut_menu.get_state_second());
  if(!(shortcut_menu.get_state_third() == nullptr)){
    shortcut_states.push_back(shortcut_menu.get_state_third());
  }
  if(!(shortcut_menu.get_state_fourth() == nullptr)){
    shortcut_states.push_back(shortcut_menu.get_state_fourth());
  }
  if(!(shortcut_menu.get_state_fifth() == nullptr)){
    shortcut_states.push_back(shortcut_menu.get_state_fifth());
  }
  if(!(shortcut_menu.get_state_sixth() == nullptr)){
    shortcut_states.push_back(shortcut_menu.get_state_sixth());
  }

  for(int i=0; i < states_array.size(); ++i){
        if(states_array.at(i).get_gait_name().empty()){
            states_array.at(i).set_shortcut_push(shortcut_states.at(0));
        }
    }
}