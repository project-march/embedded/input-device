#include "menu.h"

StartMenu::StartMenu(State* const state_first, State* const state_second, State* const state_third, State* const state_fourth) :
    state_first_(state_first), state_second_(state_second), state_third_(state_third), state_fourth_(state_fourth)
{
    set_left_right(state_first_, state_second_);
    if(state_third_ != nullptr){
        set_left_right(state_second_, state_third_);
        if(state_fourth_ != nullptr){
            set_left_right(state_third_, state_fourth_);
        }
    }
}

StartMenuLarge::StartMenuLarge(State* const state_first, State* const state_second, State* const state_third, State* const state_fourth, State* const state_fifth, State* const state_sixth, State* const state_seventh, State* const state_eighth, State* const state_ninth, State* const state_tenth) :
    StartMenu(state_first, state_second, state_third, state_fourth), state_fifth_(state_fifth), state_sixth_(state_sixth), state_seventh_(state_seventh), state_eighth_(state_eighth), state_ninth_(state_ninth), state_tenth_(state_tenth)
{
    set_left_right(state_fourth_, state_fifth_);
    if(state_sixth_ != nullptr){
        set_left_right(state_fifth_, state_sixth_);
        if(state_seventh_ != nullptr){
            set_left_right(state_sixth_, state_seventh_);
            if(state_eighth_ != nullptr){
                set_left_right(state_seventh_, state_eighth_);
                if(state_ninth_ != nullptr){
                    set_left_right(state_eighth, state_ninth_);
                    if(state_tenth_ != nullptr){
                        set_left_right(state_ninth_, state_tenth_);
                    }
                }
            }
        }
    }
}


Menu::Menu(State* const state_menu_select, State* const state_first, State* const state_second, State* const state_third, State* const state_fourth) :
    StartMenu(state_first, state_second, state_third, state_fourth), state_menu_select_(state_menu_select)
{
    set_select_back(state_menu_select_, state_first_);
    set_back(state_second_, state_menu_select_);
    if(state_third_ != nullptr) {
        set_back(state_third_, state_menu_select);
        if(state_fourth_ != nullptr){
            set_back(state_fourth_, state_menu_select_);
        }
    }
}

StartMenu::StartMenu(State* const state_turn_off, GaitSequence* const gait_first, GaitSequence* const gait_second, Menu* const menu) :
    StartMenu(state_turn_off, gait_first->get_state_gait_menu(), gait_second->get_state_gait_menu(), menu->get_state_menu_select())
{
}

Menu::Menu(Menu* const menu_first, Menu* const menu_second, Menu* const menu_third, Menu* const menu_fourth) :
    Menu(menu_first->get_state_menu_select(), menu_second->get_state_menu_select(), menu_third->get_state_menu_select(), menu_fourth->get_state_menu_select())
{
}

MenuLarge::MenuLarge(State* const state_menu_select, State* const state_first, State* const state_second, State* const state_third, State* const state_fourth, State* const state_fifth, State* const state_sixth, State* const state_seventh, State* const state_eight) :
    Menu(state_menu_select, state_first, state_second, state_third, state_fourth), 
    state_fifth_(state_fifth), state_sixth_(state_sixth), state_seventh_(state_seventh), state_eight_(state_eight)
{
    set_left_right(state_fourth_, state_fifth_);
    set_back(state_fifth_, state_menu_select_);
    if(state_sixth_ != nullptr){
        set_left_right(state_fifth_, state_sixth_);
        set_back(state_sixth_, state_menu_select_);
        if(state_seventh_ != nullptr){
            set_left_right(state_sixth_, state_seventh_);
            set_back(state_seventh_, state_menu_select_);
            if(state_eight_ != nullptr){
                set_left_right(state_seventh_, state_eight_);
                set_back(state_eight_, state_menu_select_);
            }
        }
    }
}

ShortcutMenu::ShortcutMenu(State* const state_first, State* const state_second, State* const state_third, State* const state_fourth, State* const state_fifth, State* const state_sixth) :
    StartMenu(state_first, state_second, state_third, state_fourth), state_fifth_(state_fifth), state_sixth_(state_sixth)
{
    if(!(state_fifth_ == nullptr)){
        set_left_right(state_fourth_, state_fifth_);
        state_fifth_->set_in_shortcut_menu(true);
        if(!(state_sixth_ == nullptr)){
            set_left_right(state_fifth_, state_sixth_);
            state_sixth_->set_in_shortcut_menu(true);
        }
    }
    if(!(state_third_ == nullptr)){
        state_third_->set_in_shortcut_menu(true);
        if(!(state_fourth_ == nullptr)){
            state_fourth_->set_in_shortcut_menu(true);
        }
    }
    state_first_->set_in_shortcut_menu(true);
    state_second_->set_in_shortcut_menu(true);
}