import yaml
import copy

gaits = list()

class Gait:
    def __init__(self, name, gait_name, on_completion):
        self.name = name
        self.gait_name = gait_name
        self.on_completion = on_completion
        self.menu_prefix = ""

#for menu elements that are neither a gait nor a menu
class Other:
    def __init__(self, name):
        self.name = name

def add_gait(name, gait_dict):
    if("other" in gait_dict):
        gait = Other(name)
        gaits.append(gait)
        return
    gait_name = gait_dict["gait_name"]
    on_completion = gait_dict["on_completion"]
    gait = Gait(name, gait_name, on_completion)
    gaits.append(gait)

menus = list()

class StartMenu:
    def __init__(self, name, elements):
        self.name = name + "_menu"
        self.elements = elements

class Menu(StartMenu):
    def __init__(self, name, menu_select, elements):
        StartMenu.__init__(self, name, elements)
        self.menu_select = menu_select + "_menu"

class ShortcutMenu(StartMenu):
    def __init__(self, name, alias, elements):
        StartMenu.__init__(self, name, elements)
        self.alias = alias + "_menu"

def add_menu(name, menu_dict):
    elements = [menu_dict["state_first"], menu_dict["state_second"]]
    if "state_third" in menu_dict:
        elements.append(menu_dict["state_third"])
    if "state_fourth" in menu_dict:
        elements.append(menu_dict["state_fourth"])
    if "state_fifth" in menu_dict:
        elements.append(menu_dict["state_fifth"])
    if "state_sixth" in menu_dict:
        elements.append(menu_dict["state_sixth"])
    if "state_seventh" in menu_dict:
        elements.append(menu_dict["state_seventh"])
    if "state_eighth" in menu_dict:
        elements.append(menu_dict["state_eighth"])
    if "state_ninth" in menu_dict:
        elements.append(menu_dict["state_ninth"])
    if "state_tenth" in menu_dict:
        elements.append(menu_dict["state_tenth"])

    if(name == "start"):
        menu = StartMenu(name, elements)
    elif(name == "shortcut"):
        menu = ShortcutMenu(name, menu_dict["alias"], elements)
    else:
        menu = Menu(name, menu_dict["menu_select"], elements)
    
    menus.append(menu)

def complete_gait_names(state_machine_prefix, gaits, menus):
    for menu in menus:
        for gait in gaits:
            if(gait.name in menu.elements):
                if(hasattr(menu, "alias")):
                    extended_name = "_".join([state_machine_prefix, menu.alias, gait.name])
                else:
                    extended_name = "_".join([state_machine_prefix, menu.name, gait.name])
                menu.elements[menu.elements.index(gait.name)] = extended_name
                gait.name = extended_name

def complete_menu_names(state_machine_prefix, menus):
    for menu in menus:
        if(menu.name == "start_menu"):
            menu.name = "_".join([state_machine_prefix, menu.name])
        elif(menu.name == "shortcut_menu"):
            menu.name = "_".join([state_machine_prefix, menu.alias])
        else:
            for upper_menu in menus:
                if(menu.name in upper_menu.elements):
                    extended_name = "_".join([state_machine_prefix, menu.menu_select, menu.name])
                    upper_menu.elements[upper_menu.elements.index(menu.name)] = extended_name
                    menu.name = extended_name

def complete_on_completion_names(gaits, menus):
    for gait in gaits:
        if(not hasattr(gait, "on_completion")):
            continue
        name_length = len(gait.on_completion)
        if(gait.on_completion[-4:] == "menu"):
            for menu in menus:
                if(menu.name[-name_length:] == gait.on_completion):
                    gait.on_completion = menu.name
        else:
            for gait_search in gaits:
                if(gait_search.name[-name_length:] == gait.on_completion):
                    gait.on_completion = gait_search.name

def construct_gaits(gaits):
    gaits_without_dummies = copy.deepcopy(gaits)
    for gait in gaits_without_dummies:
        if(not hasattr(gait, "on_completion")):
            gaits_without_dummies.remove(gait)
    gait_list = "\n".join([f"GaitSequence {gait.name.lower()}(&states_array[{gait.name.upper()}], &states_array[{gait.name.upper()}_SELECTED], &states_array[{gait.name.upper()}_ACTIVATED], &states_array[{gait.on_completion.upper()}]);" for gait in gaits_without_dummies])
    return gait_list

def construct_menus(menus):
    first_iter = True
    for menu in menus:
        if(not hasattr(menu, "menu_select")):
            if(not hasattr(menu, "alias")):
                if(len(menu.elements) > 4):
                    class_syntax = "StartMenuLarge"
                else:
                    class_syntax = "StartMenu"
            else:
                class_syntax = "ShortcutMenu"
        elif(len(menu.elements) > 4):
            class_syntax = "MenuLarge"
        else:
            class_syntax = "Menu"

        construct_menu = ", ".join([f"&states_array.at({element.upper()})" for element in menu.elements])
        if(hasattr(menu, "menu_select")):
            construct_menu = ", ".join([f"&states_array.at({menu.name.upper()})", construct_menu])
        if(first_iter):
            menu_list = f"{class_syntax} {menu.name.lower()}({construct_menu});"
            first_iter = False
        else:
            menu_list = "\n".join([menu_list, f"{class_syntax} {menu.name.lower()}({construct_menu});"])
    return menu_list 

def return_statement(menus):
    for menu in menus:
        if(hasattr(menu, "alias")):
            shortcut_menu_name = f"return {menu.name.lower()};"
            break
    return shortcut_menu_name

with open("state_machine_reveal.yaml", "r") as stream:
    try:
        dictionary = yaml.safe_load(stream)
        statemachine_name = dictionary["statemachine"]

        for gait in dictionary["gaits"]:
            add_gait(gait, dictionary["gaits"][gait])
        
        add_menu("start", dictionary["start"])
        add_menu("shortcut", dictionary["shortcut"])

        for menu in dictionary["menus"]:
            menu_dict = dictionary[menu]
            add_menu(menu, menu_dict)

    except yaml.YAMLError as exc:
        print(exc)

complete_gait_names(statemachine_name, gaits, menus)
complete_menu_names(statemachine_name, menus)
complete_on_completion_names(gaits, menus)

test_file = open('../include/state_machine_construction.h', 'w')
output = test_file.write

output("""#ifndef STATE_MACHINE_CONSTRUCTION_H
#define STATE_MACHINE_CONSTRUCTION_H


// This file is auto-generated using generate_state_machine.py
// Don't edit this file by hand. Always use the generator script!

#include "state_machine.h"
#include "sd_sector_addresses.h"
#include "states_array.h"
#include "menu.h"

ShortcutMenu construct_statemachine()
{
    //All gait sequences are constructed
""")
output(construct_gaits(gaits))
output("""
    
    //All menus are constructed
""")
output(construct_menus(menus))
output(return_statement(menus))
output("""

}

#endif

""")
output("\n")
