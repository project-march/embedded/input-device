import yaml
import copy

sector_addresses = list()

class SectorAddress:
    def __init__(self, name, high_address, low_address):
        self.name = name
        self.high_address = high_address
        self.low_address = low_address

    def __lt__(self, other):
        return self.name < other.name

selected_and_activated_equal_to_normal = True
overwrite_header_file = True

# Load list of all images that are required on the input device, any missing image will be defined as the image on the first address,
# this should be the not_implemented image
required_images = set()
# with open("required_images.yaml", "r") as stream:
#     try:
#         dictionary = yaml.safe_load(stream)
#         for gait in dictionary['gaits']:
#             required_images.add(gait)
#             required_images.add(f"{gait}_SELECTED")
#             required_images.add(f"{gait}_ACTIVATED")
#         for image in dictionary['other']:
#             required_images.add(image)
#     except yaml.YAMLError as exc:
#         print(exc)

from generate_state_machine import gaits
from generate_state_machine import menus

def define_required_images():
    for menu in menus:
        required_images.add(menu.name.upper())
    for gait in gaits:
        if(hasattr(gait, "gait_name")):
            required_images.add(gait.name.upper())
            required_images.add(f"{gait.name.upper()}_SELECTED")
            required_images.add(f"{gait.name.upper()}_ACTIVATED")
        else:
            required_images.add(gait.name.upper())

def add_sector_address(name, address):
    if name in required_images:
        required_images.remove(name)
        # Split the address notation into the start and end address
        address = address.split(", ")
        high_address = address[0]
        low_address = address[1]

        # Append the SectorAddress to the list of all addresses
        sector_address = SectorAddress(name, high_address, low_address)
        sector_addresses.append(sector_address)
    else:
        print(f"Not required image present: {name}")

def sector_address_enum(sector_addresses):
    indices = ",\n".join(["  " + x.name for x in sector_addresses])
    return "enum StatesEnum {\n" + indices + "\n};"

def sector_name_array(sector_addresses):
    names = ",\n".join([f"  \"{x.name}\\0\"" for x in sector_addresses])
    return "static constexpr std::array<const char*, " + str(len(sector_addresses)) + "> STATE_NAMES {\n" + names + "\n};"

def sector_address_array(sector_addresses):
    addresses = ",\n".join([f"  SectorAddress {{{x.high_address}, {x.low_address}}}" for x in sector_addresses])
    return "static constexpr std::array<SectorAddress, " + str(len(sector_addresses)) + "> SECTOR_ADDRESSES {\n" + addresses + "\n};"

def states_array(gaits, sector_address):
    not_first = False
    for x in sector_address:
        if("ACTIVATED" in x.name):
            changed = False
            for gait in gaits:
                if(changed == True):
                    break
                if(x.name[:-10].lower() == gait.name):
                    state = f"  State({x.name}, \"{gait.gait_name}\")"
                    changed = True
            if(changed == False):
                index = x.name.find("_MENU_")
                name_of_gait = x.name[index+6:-10].lower()
                state = f"  State({x.name}, \"{name_of_gait}\")"
        else:
            state = f"  State({x.name})"
        if(not_first):   
            states = ",\n".join([states, state])
        else:
            states = state
            not_first = True
        #print(states)
    return "static std::array<State, " + str(len(sector_addresses)) + "> states_array {\n" + states + "\n};"


# If not implemented is not specifically given, use the first image available
not_implemented_address = "0x0000, 0x0000" 

gait_addresses = {}
define_required_images()
# After uploading the images, move the .Gc to this directory and change the name
with open("ipd-addresses.Gc", "r") as sector_file:
    for line in sector_file:
        if line[0:9] == "#constant":
            gait_name = line.split(" ")[1].replace(".png", "").replace(".bmp", "").upper()

            address = line.split("$media_SetSector(")[1].split(");")[0]
            add_sector_address(gait_name, address)

            if gait_name == "NOT_IMPLEMENTED":
                not_implemented_address = address

            # Also save the address in a dict to use for filling missing selected and activated
            gait_addresses[gait_name] = address
                
# Set all missing images to the not implemented address:
for image in copy.deepcopy(required_images):
    print(f"Missing required image {image}")
    if selected_and_activated_equal_to_normal:
        if "SELECTED" in image:
            gait_name = image.replace("_SELECTED", "")
            if gait_name in gait_addresses.keys():
                add_sector_address(image, gait_addresses[gait_name])
                continue
        if "ACTIVATED" in image:
            gait_name = image.replace("_ACTIVATED", "")
            if gait_name in gait_addresses.keys():
                add_sector_address(image, gait_addresses[gait_name])
                continue
    
    add_sector_address(image, not_implemented_address)

# Sort the addresses based on their name for consistent ordering
sector_addresses = sorted(sector_addresses)

no_state = SectorAddress("NO_STATE", 0, 0)
sector_addresses.insert(0, no_state)

if overwrite_header_file:
    sector_addresses_file = open('../include/sd_sector_addresses.h', 'w')
    output = sector_addresses_file.write
else:
    # Print the result, this can be copy-pasted into the sd_sector_addresses.h to update ipd images
    output = print

output("""// This file is auto-generated using get_sector_addresses.py
// Don't edit this file by hand. Always use the generator script!

#include <array>

#ifndef SD_SECTOR_ADDRESSES_H
#define SD_SECTOR_ADDRESSES_H

// This file defines the sector addresses of the images to be loaded on the
// screen The sector addresses can be found via the 4D Systems Workshop 4D IDE
// The Graphics Composer of this software should be used:
//  - To load the desired images on the uSD card
//  - To find the sector addresses of said images (via generated .Gc file)

// A sector address on the SD card is defined by
// an address consisting of two parts: high and low bits.
// Both parts specify 2 bytes of the complete address
// So, address = hi << 8 | lo
// These two are split, since that is how the 4dsystems
// serial library addresses images on the SD.
struct SectorAddress
{
  unsigned int hi;
  unsigned int lo;
};

// clang-format off
""")
output(sector_address_enum(sector_addresses))
output("\n")
output(sector_name_array(sector_addresses))
output("\n")
output(sector_address_array(sector_addresses))
output("""// clang-format on

#endif  // SD_SECTOR_ADDRESSES_H

""")

if overwrite_header_file:
    sector_addresses_file.close()

if overwrite_header_file:
    states_array_file = open('../include/states_array.h', 'w')
    output = states_array_file.write
else:
    # Print the result, this can be copy-pasted into the sd_sector_addresses.h to update ipd images
    output = print

output("""// This file is auto-generated using get_sector_addresses.py
// Don't edit this file by hand. Always use the generator script!

#ifndef STATES_ARRAY_H
#define STATES_ARRAY_H

#include <array>
#include "state.h"

//This file contains all the states used by the statemachine. 
//Which states should be generated can be selected by setting the accompanying images in required_images.yaml

// clang-format off
""")
output(states_array(gaits, sector_addresses))
# output("""

# #include "gait_sequence.h"

# """)
# output(gait_sequences(sector_addresses))
output("""// clang-format on

#endif //STATES_ARRAY_H

""")

