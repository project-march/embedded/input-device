#ifndef STATE_MACHINE_NEW_H
#define STATE_MACHINE_NEW_H

#include "state.h"
#include "sd_sector_addresses.h"
#include "menu.h"
#include "gait_sequence.h"
#include <vector>

#include <string>

class StateMachine
{
public:
  StateMachine() = default;

  void construct();

  const std::string get_current_gait_name() const;
  const char* get_current_state_name() const;
  const SectorAddress& get_current_image() const;

  const size_t size() const;

  bool left();
  bool right();
  bool shortcut_push();

  bool back();
  bool select();
  bool activate();

private:
  bool has_state() const;
  bool set_current_state(const State* new_state);

  const State* current_state_ = nullptr;
  const State* previous_state_ = nullptr;
  // ShortcutMenu shortcut_menu_ = nullptr;
  StartMenu* const start_menu_ = nullptr;
  std::vector<State*> shortcut_states;
};

#endif  // STATE_MACHINE_H
