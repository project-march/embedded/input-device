#ifndef MENU_H
#define MENU_H

#include "state_linker.h"
#include "gait_sequence.h"

class Menu;

class StartMenu : public StateLinker
{
public:
    StartMenu(State* const state_first, State* const state_second, State* const state_third = nullptr, State* const state_fourth = nullptr);
    StartMenu(State* const state_turn_off, GaitSequence* const gait_first, GaitSequence* const gait_second, Menu* const menu);
protected:
    State* const state_first_;
    State* const state_second_;
    State* const state_third_;
    State* const state_fourth_;
};

class StartMenuLarge : public StartMenu
{
public:
    StartMenuLarge(State* const state_first, State* const state_second, State* const state_third, State* const state_fourth, State* const state_fifth, State* const state_sixth = nullptr, State* const state_seventh = nullptr, State* const state_eighth = nullptr, State* const state_ninth = nullptr, State* const state_tenth = nullptr);
private:
    State* const state_fifth_;
    State* const state_sixth_;
    State* const state_seventh_;
    State* const state_eighth_;
    State* const state_ninth_;
    State* const state_tenth_;
};

class Menu : public StartMenu
{
public:
    Menu(State* const state_menu_select, State* const state_first, State* const state_second, State* const state_third = nullptr, State* const state_fourth = nullptr);
    Menu(Menu* const menu_first, Menu* const menu_second, Menu* const menu_third = nullptr, Menu* const menu_fourth = nullptr);

    State* const get_state_menu_select() const { return state_menu_select_; };
protected:
    State* const state_menu_select_;
};

class MenuLarge : public Menu
{
public:
    MenuLarge(State* const state_menu_select, State* const state_first, State* const state_second, State* const state_third, State* const state_fourth, State* const state_fifth, State* const state_sixth = nullptr, State* const state_seventh = nullptr, State* const state_eight = nullptr);

private:
    State* const state_fifth_;
    State* const state_sixth_;
    State* const state_seventh_;
    State* const state_eight_;
};

class ShortcutMenu: public StartMenu
{
public:
    ShortcutMenu(State* const state_first, State* const state_second, State* const state_third = nullptr, State* const state_fourth = nullptr, State* const state_fifth =  nullptr, State* const state_sixth = nullptr);

    // void set_state_return(const State* state_return) { 
    //     state_first_->set_back(state_return);
    //     state_second_->set_back(state_return);
    //     state_third_->set_back(state_return);
    //     state_fourth_->set_back(state_return);
    // }

    State* const get_state_first() {
        return state_first_;
    }

    State* const get_state_second() {
        return state_second_;
    }

    State* const get_state_third() {
        return state_third_;
    }

    State* const get_state_fourth() {
        return state_fourth_;
    }

    State* const get_state_fifth() {
        return state_fifth_;
    }

    State* const get_state_sixth(){
        return state_sixth_;
    }

private:
    State* const state_fifth_;
    State* const state_sixth_;
};

#endif // MENU_H