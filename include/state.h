#ifndef STATE_H
#define STATE_H

#include "sd_sector_addresses.h"

#include <string>

class State
{
public:
  State(const StatesEnum state, const std::string& gait) : gait_(gait), state_(state)
  {
  }

  explicit State(StatesEnum state) : State(state, "")
  {
  }

  void set_left(const State* state_with_left) { left_ = state_with_left; }
  void set_right(const State* state_with_right) { right_ = state_with_right; }
  void set_shortcut_push(const State* state_with_shortcut_push) { shortcut_push_ = state_with_shortcut_push; }
  void set_shortcut_double_push(const State* state_with_shortcut_double_push) { shortcut_double_push_ = state_with_shortcut_double_push; }
  void set_back(const State* state_with_back) { back_ = state_with_back; }
  void set_select(const State* state_with_select) { select_ = state_with_select; }
  void set_activate(const State* state_with_activate) { activate_ = state_with_activate; }
  void set_in_shortcut_menu(bool value) { in_shortcut_menu_ = value; }

  const State* get_left() const { return left_; }
  const State* get_right() const { return right_; }
  const State* get_shortcut_push() const { return shortcut_push_; }
  const State* get_shotcut_double_push() const { return shortcut_double_push_; }
  const State* get_back() const { return back_; }
  const State* get_select() const { return select_; }
  const State* get_activate() const { return activate_; }
  const bool get_in_shortcut_menu() const { return in_shortcut_menu_; }
  const std::string& get_gait_name() const {return gait_; }
  const SectorAddress& get_image() const { return SECTOR_ADDRESSES[state_]; }
  const char* get_raw_state_name() const { return STATE_NAMES[state_]; }

private:
  const std::string gait_;
  const StatesEnum state_;

  const State* left_ = this;
  const State* right_ = this;
  const State* shortcut_push_ = this;
  const State* shortcut_double_push_ = this;

  const State* back_ = this;
  const State* select_ = this;
  const State* activate_ = this;

  bool in_shortcut_menu_ = false;
};

#endif  // STATE_H
