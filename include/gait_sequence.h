#ifndef GAIT_SEQUENCE_H
#define GAIT_SEQUENCE_H

#include "state_linker.h"

class GaitSequence : public StateLinker
{
public:
    GaitSequence(State* const state_gait_menu, State* const state_gait_selected, State* const state_gait_activated, State* const state_upon_completion) : 
        state_gait_menu_(state_gait_menu), state_gait_selected_(state_gait_selected), state_gait_activated_(state_gait_activated), state_upon_completion_(state_upon_completion)
        {
            set_select_back(state_gait_menu_, state_gait_selected_);
            set_activate(state_gait_selected_, state_gait_activated_);
            set_activate(state_gait_activated_, state_upon_completion_);            
        }

    State* const get_state_gait_menu() const { return state_gait_menu_; }
private:
    State* const state_gait_menu_;
    State* const state_gait_selected_;
    State* const state_gait_activated_;
    State* const state_upon_completion_;
};

#endif