#ifndef STATE_LINKER_H
#define STATE_LINKER_H

#include "state.h"

class StateLinker
{
public:
    void set_right(State* const state_initial, const State* const state_with_right) { state_initial->set_right(state_with_right); }
    void set_left(State* const state_initial, const State* const state_with_left) { state_initial->set_left(state_with_left); }
    void set_left_right(State* const state_left, State* const state_right) { set_right(state_left, state_right); set_left(state_right, state_left); }
    void set_select(State* const state_initial, const State* const state_with_select) { state_initial->set_select(state_with_select); }
    void set_back(State* const state_initial, const State* const state_with_back) { state_initial->set_back(state_with_back); }
    void set_select_back(State* const state_back, State* const state_select) { set_select(state_back, state_select), set_back(state_select, state_back); }
    void set_activate(State* const state_initial, const State* const state_with_activate) { state_initial->set_activate(state_with_activate); }
    void set_shortcut_push(State* const state_initial, const State* const state_with_shortcut) { state_initial->set_shortcut_push(state_with_shortcut); }
};

#endif // STATE_LINKER_H