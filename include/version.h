#ifndef VERSION_H
#define VERSION_H

#define ORGANIZATION "Project\n\rMarch"
#define PROJECT_NAME "input-device"
#define VERSION "v5.0.0"

#endif  // VERSION_H
